package ru.t1.vlvov.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.dto.IDtoRepository;
import ru.t1.vlvov.tm.api.service.dto.IDtoService;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Getter
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected abstract IDtoRepository<M> getRepository();

    @Override
    public void clear() {
        @NotNull final IDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull Collection<M> collection) {
        @NotNull final IDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.set(collection);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<M> findAll() {
        @NotNull final IDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            return repositoryDTO.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public M findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            return getRepository().findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            getRepository().remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        M model = findOneById(id);
        remove(model);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

}
