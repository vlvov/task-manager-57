package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.model.IProjectRepository;
import ru.t1.vlvov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.model.IProjectService;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.exception.field.NameEmptyException;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @Autowired
    @NotNull
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project model = new Project();
        model.setName(name);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        model.setUser(entityManager.find(User.class, userId));
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project model = new Project();
        model.setName(name);
        model.setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        model.setUser(entityManager.find(User.class, userId));
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
