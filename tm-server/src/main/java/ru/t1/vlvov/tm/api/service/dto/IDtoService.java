package ru.t1.vlvov.tm.api.service.dto;

import ru.t1.vlvov.tm.api.repository.dto.IDtoRepository;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;

public interface IDtoService<M extends AbstractModelDTO> extends IDtoRepository<M> {
}
